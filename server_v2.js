var express = require('express');
var bodyParse = require('body-parser');
var app = express();
var port=process.env.PORT || 3000;
//var usersFile =require('./login.json');
var URLbase="/colapi/v3/";
var requestJSON = require('request-json');
var baseMlabURL= 'https://api.mlab.com/api/1/databases/colpaidb_cconde/collections/';
var apiKeyMalb = 'apiKey=dPVr4KN0zCKuR0fJXGDfYLsoA1UfebXL';


app.listen(port);
app.use(bodyParse.json());
console.log('Colapi escuchando en puerto ' +  port);

//acceso desde mlab login
app.get(URLbase + 'users',
  function(req, res){
  console.log("Entra al GET");

  var httpClient = requestJSON.createClient(baseMlabURL);
  console.log("Cliente HTTP mlab creado");
  var comodin='f={"_id":0}&';
  httpClient.get('user?' + comodin + apiKeyMalb,
   function(err, respuestaMlab,body){
      console.log('Error' + err);
      console.log('Body ' + body);
      console.log('respuestaMlab ' + respuestaMlab);
      //var respuestas= req.params;
      res.send(body);
   });
  });

  // busca un id en mlab
  app.get(URLbase + "users/:id",
     function (req, res) {
       console.log("GET /colapi/v3/users/:id/");
       console.log(req.params.id);

       var id=req.params.id;
       var queryString = 'q={"id":' + id + '}&';
       var httpClient = requestJSON.createClient(baseMlabURL);

       httpClient.get('user?' + queryString + apiKeyMalb,
        function(err,respuestaMlab,body){
          console.log("Respuesta Mab OK...");
          var respuesta= body[0];
          res.send(respuesta);
        })
  });

///////////////////////////////////////////////////////////
//acceso desde mlab cuentas
app.get(URLbase + 'cuentas',
  function(req, res){
  console.log("Entra al GET");

  var httpClient = requestJSON.createClient(baseMlabURL);
  console.log("Cliente HTTP mlab creado");
  var comodin='f={"_id":0}&';
  httpClient.get('account?' + comodin + apiKeyMalb,
   function(err, respuestaMlab,body){
      console.log('Error' + err);
      console.log('Body ' + body);
      console.log('respuestaMlab ' + respuestaMlab);
      //var respuestas= req.params;
      res.send(body);
   });
  });

  // busca un id en mlab
  app.get(URLbase + "cuentas/:userid",
     function (req, res) {
       console.log("GET /colapi/v3/cuentas/:userid/");
       console.log(req.params.userid);

       var userid=req.params.userid;
       var queryString = 'q={"userid":' + userid + '}&';
       var httpClient = requestJSON.createClient(baseMlabURL);

       httpClient.get('account?' + queryString + apiKeyMalb,
        function(err,respuestaMlab,body){
          console.log("Respuesta Mab OK...");
          var respuesta= body[0];
          res.send(respuesta);
        })
  });

  ///////////////////////////////////////////////////////////
  //acceso desde mlab movimientos
  app.get(URLbase + 'movimientos',
    function(req, res){
    console.log("Entra al GET");

    var httpClient = requestJSON.createClient(baseMlabURL);
    console.log("Cliente HTTP mlab creado");
    var comodin='f={"_id":0}&';
    httpClient.get('movement?' + comodin + apiKeyMalb,
     function(err, respuestaMlab,body){
        console.log('Error' + err);
        console.log('Body ' + body);
        console.log('respuestaMlab ' + respuestaMlab);
        //var respuestas= req.params;
        res.send(body);
     });
    });

    // busca un id en mlab
    app.get(URLbase + "movimientos/:id_mov",
       function (req, res) {
         console.log("GET /colapi/v3/movimientos/:userid/");
         console.log(req.params.id_mov);

         var id_mov=req.params.id_mov;
         var queryString = 'q={"id_mov":' + id_mov + '}&';
         var httpClient = requestJSON.createClient(baseMlabURL);

         httpClient.get('movement?' + queryString + apiKeyMalb,
          function(err,respuestaMlab,body){
            console.log("Respuesta Mab OK...");
            var respuesta= body[0];
            res.send(respuesta);
          })
    });



///////////////////////////////////////////// post
app.post(URLbase + 'users/post',
 function(req, res) {
   console.log("Entro al post");
   clienteMlab = requestJSON.createClient(baseMlabURL + "/user?" + apiKeyMalb)
   clienteMlab.post('', req.body, function(err, resM, body) {
      console.log("se crea el clienteMlab");
      var newID = usersFile.length + 1;
      res.send(body)
   })

})


//////////////////////////MODIFICA CUENTA
  app.put(URLbase+'accounts/:id',
    function(req, res) {
     var id=req.params.id;
     var clienteMlab = requestJSON.createClient(baseMlabURL + "account");

     //modificacion, toca poner el dolar para el mlab
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     console.log('pruebas cambios'+cambio)
     clienteMlab.put('?q={"userid": ' + id + '}&' + apiKeyMalb,
       JSON.parse(cambio),
        function(err, resM, body) {
          console.log('err '+err+' resM '+ resM);
         res.send(body);
     });
 });


 //POST LOGIN
 app.post(URLbase+'login',
   function(req, res) {
    var email=req.body.email;
    var password=req.body.password;
    var id;
    var loggin=false;
    var queryString='q={"email":"'+email+'","password":"'+password+'"}&';
    console.log(queryString);

    //GET CONSULTAR PASS Y EMAIL
    var httpClient=requestJSON.createClient(baseMlabURL);
    httpClient.get('user?'+queryString+apiKeyMalb,

      function(err, respMlab, body){
        if(body[0]!=undefined&&body[0].email==email&&body[0].password==password){
          console.log("pruebas body mich "+body[0].email);
            var cambio = '{"$set":{"loggin":true}}';
            console.log('pruebas cambios '+cambio);

            //PUT agregar login
            httpClient.put('user?q={"email":"'+email+'"}&' + apiKeyMalb,
              JSON.parse(cambio),
               function(err, resM, body) {
                res.send({"msg":"Bienvenido al login de postman"});
            });
        }else{
          res.send({"msg":"Usuario o contraseña incorrecta"});
        }
      });
});



app.post(URLbase+'logout',
  function(req, res) {
   var email=req.body.email;
   //GET CONSULTAR PASS Y EMAIL
   var httpClient=requestJSON.createClient(baseMlabURL);
           //PUT agregar login
           var cambio = '{"$unset":{"loggin":""}}';
           httpClient.put('user?q={"email":"' + email + '"}&' + apiKeyMalb,
             JSON.parse(cambio),
              function(err, resM, body) {
                if(body.n=='1'){
                  console.log(body.n);
                  res.send({"msg":"sesión cerrada"});
                }else{
                  res.send({"msg":"Invalido"});
                }
           });
});
