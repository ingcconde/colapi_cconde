var mocha =require('mocha')
var chai =require('chai')
var chaiHttp = require('chai-http')
var server = ('../server')

var should = chai.should()

chai.use(chaiHttp)


describe('pruebas colombia', () => {

  it('BBVA funcion ', (done) => {
    chai.request('http://www.bbva.com')
    .get('/')
    .end((error,respuestas) => {
      //console.log(respuestas)
      respuestas.should.have.status(200)
      done()
    })
  });

   it('Mi api funciona', (done) => {
     chai.request('http://localhost:3000')
          .get('/colapi/v3')
          .end((error,respuestas) => {
            respuestas.should.have.status(200)
            done()
          })
   });

   it('Devuelve un array de usuarios', (done) => {
     chai.request('http://localhost:3000')
          .get('/colapi/v3/users')
          .end((error,respuestas) => {
            respuestas.body.should.be.a('array')
            done()
          })
   });

   it('Devuelve al menos un elemento', (done) => {
     chai.request('http://localhost:3000')
          .get('/colapi/v3/users')
          .end((error,respuestas) => {
            respuestas.body.length.should.be.gte(1)
            done()
          })
   });

   it('Devuelve dos propiedades', (done) => {
     chai.request('http://localhost:3000')
          .get('/colapi/v3/users')
          .end((error,respuestas) => {
            respuestas.body[0].should.have.property('first_name')
            respuestas.body[0].should.have.property('last_name')
            done()
          })
   });


   it('Valida crea elemento', (done) => {
     chai.request('http://localhost:3000')
          .post('/colapi/v3/users')
          .send('{"first_name":"Doraemon", "last_name":"Garcia"}')
          .end((error, respuestas, body) => {
              console.log(body);
              body.should.be.eql('exito......')
            done()
          })
   });

});
